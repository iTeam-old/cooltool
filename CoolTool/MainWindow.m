//
//  MainWindow.m
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/13/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import "MainWindow.h"
#import "BackgroudView.h"

static const CGFloat sAlphaIncrement        = 0.1f;
static const NSInteger sSmallWindowOffset   = 1.0f;
static const NSInteger sBigWindowOffset     = 10.0f;

@implementation MainWindow

- (id) initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag
{
    self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag];
    if (self)
    {
        mImageView = [[NSImageView alloc] init];
        mImageView.frame = CGRectMake(0.0f, 0.0f, 100.0f, 100.0f);
        
        [self.contentView addSubview:mImageView];
    }
    return self;
}

- (void) makeKeyWindow
{
    [super makeKeyWindow];

    // Set up image view.
	[self setContentView:mImageView];
	[mImageView setImageScaling:NSImageScaleNone];
}

- (void)keyDown:(NSEvent *)event
{
    NSLog(@"%d, %d", (int)event.modifierFlags, event.keyCode);

    switch (event.modifierFlags)
    {
        case 11534600:  // CMD modifier for cursor is pressed
        case 11534344:
            [self performCmdForCursor:event];
            break;

        case 1048584:   // CMD modifier for letters is pressed
        case 1048840:
            [self performCmdForLetters:event];
            break;

        case 10486016:  // No modifiers are pressed
        case 10485760:
            [self performNoModifiers:event];
            break;

        case 11010336:  // ALT modifier is pressed
            [self performAltModifier:event];
            break;

        default:
            break;
    }
}

#pragma mark - Public

- (void) setAlwaysOnTop:(BOOL) aAlwaysTop
{
    if (aAlwaysTop == YES)
    {
        [self setLevel:NSFloatingWindowLevel];
    }
    else
    {
        [self setLevel:NSNormalWindowLevel];
    }

    mIsAlwaysTop = aAlwaysTop;
}

#pragma mark - Private

- (void) increaseAlpha
{
    if (self.alphaValue < 1.0f)
    {
        self.alphaValue += sAlphaIncrement;
    }
}

- (void) decreaseAlpha
{
    if (self.alphaValue > sAlphaIncrement)
    {
        self.alphaValue -= sAlphaIncrement;
    }
}

#pragma mark - Key pressing handling

- (void) performCmdForCursor:(NSEvent *) event
{
    switch (event.keyCode)
    {
        case 126:   // up cursor button is pressed
            [self increaseAlpha];
            break;

        case 125:   // down cursor button is pressed
            [self decreaseAlpha];
            break;

        default:
            break;
    }
}

- (void) performCmdForLetters:(NSEvent *) event
{
    switch (event.keyCode)
    {
        case 34:    // 'i' key is pressed
            self.ignoresMouseEvents = !self.ignoresMouseEvents;
            break;

        case 17:    // 't' key is pressed
        {
            mIsAlwaysTop = !mIsAlwaysTop;
            [self setAlwaysOnTop:mIsAlwaysTop];
            break;
        }

        case 31:    // 'o' button is pressed
            [self performOpenDialog];
            break;
            
        default:
            break;
    }
}

- (void) performNoModifiers:(NSEvent *) event
{
    [self performWindowMovingWithEvent:event offset:sSmallWindowOffset];
}

- (void) performAltModifier:(NSEvent *) event
{
    [self performWindowMovingWithEvent:event offset:sBigWindowOffset];
}

- (void) performWindowMovingWithEvent:(NSEvent *)event offset:(CGFloat)aOffset
{
    switch (event.keyCode)
    {
        case 126:   // up cursor button is pressed
            [self moveWindowUp:aOffset];
            break;
            
        case 125:   // down cursor button is pressed
            [self moveWindowDown:aOffset];
            break;
            
        case 124:   // right cursor button is pressed
            [self moveWindowRight:aOffset];
            break;
            
        case 123:   // left cursor button is pressed
            [self moveWindowLeft:aOffset];
            break;
            
        default:
            break;
    }
}

- (void) performOpenDialog
{
    NSOpenPanel *openPanel = [[NSOpenPanel alloc] init];
    
    if ([openPanel runModal] == NSOKButton)
    {
        NSString *selectedFileName = [[[openPanel URLs] objectAtIndex:0] path];
        NSImage * image = [[NSImage alloc] initWithContentsOfFile:selectedFileName];
        [mImageView setImage:image];
        
		float height = [self titleBarHeight];
		mImageView.frame = NSMakeRect(0, 0, image.size.width, image.size.height);
		[self setFrame:NSMakeRect(self.frame.origin.x, self.frame.origin.y, 
								  mImageView.frame.size.width, mImageView.frame.size.height + height) display:YES];
    }
}

#pragma mark - Window moving

- (void) moveWindowUp:(NSInteger)aOffset
{
    CGPoint pt          = self.frame.origin;
    pt.y                += aOffset;
    [self setFrameOrigin:pt];
}

- (void) moveWindowDown:(NSInteger)aOffset
{
    CGPoint pt          = self.frame.origin;
    pt.y                -= aOffset;
    [self setFrameOrigin:pt];
}

- (void) moveWindowRight:(NSInteger)aOffset
{
    CGPoint pt          = self.frame.origin;
    pt.x                += aOffset;
    [self setFrameOrigin:pt];
}

- (void) moveWindowLeft:(NSInteger)aOffset
{
    CGPoint pt          = self.frame.origin;
    pt.x                -= aOffset;
    [self setFrameOrigin:pt];
}

#pragma mark - Helpers

- (float) titleBarHeight
{
    NSRect frame = self.frame;
	
    NSRect contentRect;
    contentRect = [NSWindow contentRectForFrameRect: frame styleMask:[self styleMask]];
	
    return (frame.size.height - contentRect.size.height);
}

@end
