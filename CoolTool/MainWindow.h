//
//  MainWindow.h
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/13/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MainWindow : NSWindow
{
    @private
        BOOL                    mIsAlwaysTop;
        NSImageView             * mImageView;
}

- (void) setAlwaysOnTop:(BOOL) aAlwaysTop;

@end
