//
//  AppDelegate.m
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/13/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [_window setAlwaysOnTop:YES];
}

@end
