//
//  AppDelegate.h
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/13/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "MainWindow.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet MainWindow *window;

@end
