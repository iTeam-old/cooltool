//
//  BackgroudView.m
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/14/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import "BackgroudView.h"

@implementation BackgroudView

@dynamic background;

-(void)setBackground:(NSColor *)aColor
{
    if([mBackground isEqual:aColor]) return;
    mBackground = aColor;
    
    [self setNeedsDisplay:YES];
}

- (NSColor *) background
{
    return mBackground;
}

- (void)drawRect:(NSRect)rect
{
    [mBackground set];
    NSRectFill([self bounds]);
}

@end
