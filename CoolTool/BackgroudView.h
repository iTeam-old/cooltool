//
//  BackgroudView.h
//  CoolTool
//
//  Created by Rostyslav Druzhchenko on 6/14/12.
//  Copyright (c) 2012 VLF Networks. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface BackgroudView : NSView
{
    @private
        NSColor         * mBackground;
}

@property (nonatomic, retain) NSColor * background;

@end
